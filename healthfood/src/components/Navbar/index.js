import React from 'react';
import './Navbar.css';
import {Link, NavLink} from 'react-router-dom';
export default function Navbar() {
    return (
        <div className='navbar-container'>

            <img className='navbar-img' src={require('../../img/logo.png')}/>

                         <ul className='navbar-menu'>
                             <li  className='navbar-menu-item'><NavLink to="/programs">Программы/Стоимость</NavLink></li>
                             <li className='navbar-menu-item'><NavLink to="/receipt">Рецепты</NavLink></li>
                             <li className='navbar-menu-item'><NavLink to="/experts">Мнение эксперта</NavLink></li>
                             <li className='navbar-menu-item'>Контакты/Вопросы</li>
                             <li className='navbar-menu-item'>Войти/Регистрация</li>
                         </ul>

        </div>
    );
}