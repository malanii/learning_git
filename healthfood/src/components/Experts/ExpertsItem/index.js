import React from 'react';
import '../Experts.css';
import {NavLink} from 'react-router-dom';

export default function ExpertsItem(props) {

    return (
        <div>
            <NavLink to={"/experts/" + props.id}>
                <img className='expert-img' src={require('../../../img/' + props.img)}/>
                <p>{props.name} {props.surname}</p>
            </NavLink>

        </div>
    );
}
