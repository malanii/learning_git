import React from 'react';
import ExpertsItem from "./ExpertsItem";

import './Experts.css';


export default function Experts() {

        let expertsData=[
            {name:"Оксана", surname: "Милевская", img: "1.jpg", id:1},
            {name:"Ольга", surname: "Андреева", img: "2.jpg", id:2},
            {name:"Виктор", surname: "Луговцов",  img: "3.jpg", id:3}
        ];

        let expertsElement = expertsData.map((expert) =>
            <ExpertsItem name={expert.name} surname ={expert.surname} img ={expert.img} id={expert.id}/>
        );


    return (
        <div className='experts-container'>
            {expertsElement}
        </div>
    );
}
