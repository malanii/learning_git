import React from 'react';
import './Footer.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faFacebookSquare, faInstagramSquare, faYoutube} from '@fortawesome/free-brands-svg-icons';


export default function Footer() {
    return (
        <div className='footer'>

            <img className='navbar-img' src={require('../../img/logo.png')}/>
            <div className="footer-contact">
                <p>050-112-11-11</p>
                <p>info@healthfood.com</p>
            </div>
            <div className="footer-social-media">
                <FontAwesomeIcon icon={faFacebookSquare} />
                <FontAwesomeIcon icon={faInstagramSquare} />
                <FontAwesomeIcon icon={faYoutube} />
            </div>
        </div>
    );
}