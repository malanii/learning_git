import React from 'react';
import Navbar from './../components/Navbar';
import Footer from './../components/Footer'
import Programs from './../components/Programs';
import Receipt from './../components/Receipt'
import Experts from '../components/Experts'
import './App.css';
import Route from "react-router-dom/es/Route";
import {BrowserRouter} from "react-router-dom";

const App = () => {
    return (
        <BrowserRouter>
            <div>
                <Navbar/>

                <div>
                    <Route exact path="/programs" component={Programs}/>
                    <Route exact path="/receipt" component={Receipt}/>
                    <Route exact path="/experts" component={Experts}/>

                </div>
                <footer>
                    <Footer/>
                </footer>
            </div>
        </BrowserRouter>
    )
};
export default App;